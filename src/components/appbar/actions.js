import { Divider, ListItemButton, ListItemIcon, Badge } from "@mui/material";
import { ActionIconsContainerDesktop, ActionIconsContainerMobile, MyList } from "../../styles/appbar";
import PersonIcon from "@mui/icons-material/Person";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import LogoutIcon from '@mui/icons-material/Logout';
import { Colors } from "../../styles/theme";
import { useContext} from 'react';
import UserContext from '../../UserContext';
import {Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useUIContext } from "../../context/ui";

export default function Actions({ matches }) {


  const Component = matches ? ActionIconsContainerMobile : ActionIconsContainerDesktop;
  const { user } = useContext(UserContext);
  const { cart, setShowCart } = useUIContext();


  return (
    <Component>
      <MyList type="row">
        <ListItemButton
          sx={{
            justifyContent: "center",
          }}
        >
          <ListItemIcon
            sx={{
              display: "flex",
              justifyContent: "center",
              color: matches && Colors.secondary,
            }}
          >
            <Badge badgeContent={cart && cart.length} color ="secondary">
            <ShoppingCartIcon onClick={() => setShowCart(true)}/>
            </Badge>
            
          </ListItemIcon>
        </ListItemButton>
        <Divider orientation="vertical" flexItem />
        <ListItemButton
          sx={{
            justifyContent: "center",
          }}
        >
          <ListItemIcon
            sx={{
              display: "flex",
              justifyContent: "center",
              color: matches && Colors.secondary,
            }}
          >
            <FavoriteIcon />
          </ListItemIcon>
        </ListItemButton>
        <Divider orientation="vertical" flexItem />
        {(user.accessToken !== null) ?
          <>
          <ListItemButton
            sx={{
              justifyContent: "center",
            }}
          >
            <ListItemIcon
              sx={{
                display: "flex",
                justifyContent: "center",
                color: matches && Colors.secondary,
              }}
            >
              <Nav.Link as={Link} to="/logout">
                
                <LogoutIcon />
              </Nav.Link>

            </ListItemIcon>
          </ListItemButton>
          <Divider orientation="vertical" flexItem />
          </>
        
          // <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
          :
          <>
          {/* <Nav.Link as={Link} to="/login">
            Log in
          </Nav.Link> */}
          <ListItemButton
            sx={{
              justifyContent: "center",
            }}
          >
            <ListItemIcon
              sx={{
                display: "flex",
                justifyContent: "center",
                color: matches && Colors.secondary,
              }}
            >
              <Nav.Link as={Link} to="/register">
                {/* Login */}
                <PersonIcon />
              </Nav.Link>

            </ListItemIcon>
          </ListItemButton>
          <Divider orientation="vertical" flexItem />
          </>
        }



        
        
      </MyList>
    </Component>
  );
}
